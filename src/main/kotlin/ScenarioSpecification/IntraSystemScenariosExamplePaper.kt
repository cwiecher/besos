package ScenarioSpecification

import org.scenariotools.smlk.*


val positionMap = mapOf<String, Location>("Essen" to Location(1212.212121, 32.12121), "Dortmund" to Location(3232.32323, 3232.32323))
fun getLocation(name:String) = positionMap[name]!!
fun calculateRoute(fromLoc:Location, toLoc:Location) = Route(listOf(fromLoc,toLoc))

fun guaranteeScenariosRps(routeReceiver: RouteReceiver,
                        rps: Rps) = listOf(
    scenario(routeReceiver sends rps receives Rps::calculateRoute){
        val fromLocString = it.parameters[0] as String
        val toLocString = it.parameters[1] as String
        request(rpsController sends gpsService.getLocations(fromLocString, toLocString))
        val fromLoc = getLocation(fromLocString)
        val toLoc = getLocation(toLocString)
        request(gpsService sends rpsController.locations(fromLoc, toLoc))
        request(rpsController sends routePlaner.calculateRoute(fromLoc, toLoc))
        val route = calculateRoute(fromLoc, toLoc)
        request(routePlaner sends rpsController.calculatedRoute(route))
        request(rpsController sends routeReceiver.calculateRouteResponse(route))
    }
)



fun createScenarioProgram(app: App,
                           rps: Rps) : ScenarioProgram

{
    val scenarioProgram = ScenarioProgram(
        "Specification Model CS",
        isControlledObject = { o: Any -> o is Rps },
        eventNodeInputBufferSize = 25
    )

    scenarioProgram.addEnvironmentMessageType(
        //rps::calculateRoute
    )


    scenarioProgram.activeGuaranteeScenarios.addAll(
        guaranteeScenariosRps(
            app,
            rps

        )
    )
    return scenarioProgram
}