
import io.cucumber.junit.Cucumber
import io.cucumber.junit.CucumberOptions
import org.junit.runner.RunWith

@RunWith(Cucumber::class)
@CucumberOptions(
    features = ["src/main/kotlin/FeatureSpecification/"],
    //tags = ["@SoSTest"],
    //tags = ["@CsTest"],
    plugin = ["pretty", "json:target/cucumber-reports/Cucumber.json"],
    monochrome = true,
    strict = true
)

class RunTest
