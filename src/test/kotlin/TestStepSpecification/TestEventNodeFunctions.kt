 package TestStepSpecification

import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withTimeout
import org.junit.Assert
import org.scenariotools.smlk.*
import TestConfiguration.*


fun trigger(event: Event){
    runBlocking {
        testEventNode.send(event)
    }
}

fun receive(events: IEventSet){
    return runBlocking {
        if (testEventNode.receive() != events) return@runBlocking Assert.fail()
    }
}

fun eventually(events: IEventSet, forbiddenEvent: IEventSet) {
    return runBlocking {
        do {
            val receivedEvent = testEventNode.receive()
            if(receivedEvent == forbiddenEvent)
                return@runBlocking Assert.fail()
        } while (receivedEvent != events)
    }
}

fun eventually(events: IEventSet) {
    return runBlocking  {
        try{
            withTimeout(2000){
                do {
                    val receivedEvent = testEventNode.receive()
                } while (receivedEvent != events)
            }
        }catch (e: kotlinx.coroutines.TimeoutCancellationException){
            Assert.fail()
        }
    }
}

